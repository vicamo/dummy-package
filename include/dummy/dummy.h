#ifndef __DUMMY_DUMMY_H__
#define __DUMMY_DUMMY_H__

#if defined(__cplusplus)
extern "C" {
#endif

int dummy_nothing();

#if defined(__cplusplus)
}
#endif

#endif /* __DUMMY_DUMMY_H__ */
