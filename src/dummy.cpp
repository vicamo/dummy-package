#include <cstdio>
#include <cmath>
#include <vector>

#include "dummy/dummy.h"

using namespace std;

int dummy_nothing()
{
    vector<double> v;

    v.push_back(sin(M_PI / 2));
    return printf("%f\n", v.front());
}
